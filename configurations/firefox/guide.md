1. Navigate to `about:config`
2. Set `toolkit.legacyUserProfileCustomizations.stylesheets` to `true`
3. Link the css to `$HOME/.mozilla/firefox/<6ghdoubm.dev-edition-default/chrome/userChrome.css>`
4. If using high dpi screen, change `layout.css.devPixelsPerPx` to a value > 1
