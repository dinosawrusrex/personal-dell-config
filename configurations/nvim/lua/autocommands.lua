-- Indentation settings for HTML, CSS, and JS files
vim.api.nvim_create_autocmd({'BufNewFile', 'BufRead', 'BufEnter', 'BufWinEnter', 'WinEnter'}, {
    desc = 'Update tab stops for HTML, JS, and CSS',
    pattern = {'*.js', '*.jsx', '*.html', '*.css', '*.scss', '*.ts', '*.tsx'},
    callback = function()
        vim.bo.tabstop = 2
        vim.bo.softtabstop = 2
        vim.bo.shiftwidth = 2
    end,
    group = MyAuGroup
})

--[[
" PEP8 Indentation
au BufNewFile,BufRead *.py
  \ let g:pyindent_open_paren = 'shiftwidth()' |
  \ let g:pyindent_continue = '' |
--]]


-- Equalize all window dimensions on terminal resize
vim.api.nvim_create_autocmd({'VimResized'}, {
    desc = 'Equalize window dimensions on terminal resize',
    callback = function()
        if table.getn(vim.api.nvim_list_tabpages()) == 1 then
            vim.cmd.wincmd('=')
        else
            current_tab = vim.api.nvim_tabpage_get_number(0)
            vim.cmd.tabdo('wincmd =')
            vim.cmd.tabn(current_tab)
        end
    end,
    group = MyAuGroup
})


-- Clear message line
vim.api.nvim_create_autocmd('CursorMoved', {
    desc = 'Clear command line status after a command when cursor moves',
    command = 'echo',
    group = MyAuGroup
})


-- Little something for compiling Latex
vim.api.nvim_create_autocmd({'BufNewFile', 'BufRead', 'BufEnter', 'BufWinEnter', 'WinEnter'}, {
    desc = 'Shortcut to run make for compiling Latex files',
    pattern = '*.tex',
    callback = function()
        map('n', '<F2>', function() vim.api.nvim_command('make') end)
    end,
    group = MyAuGroup,
})

