vim.api.nvim_create_autocmd('TabNew', {
    desc = 'Configure tab text and styling',
    callback = function()
        vim.cmd [[
            function! Tabline()
            let s = ''
            for i in range(tabpagenr('$'))
                let tab = i + 1
                let winnr = tabpagewinnr(tab)
                let buflist = tabpagebuflist(tab)
                let bufnr = buflist[winnr - 1]
                let bufname = bufname(bufnr)
                let bufmodified = getbufvar(bufnr, "&mod")

                " Create the tab and use the appropriate highlighting
                let s .= '%' . tab . 'T'
                let s .= (tab == tabpagenr() ? '%#TabLineSel#' : '%#TabLine#')

                " Add tab number, then name
                let s .= ' ' . tab . ' '

                " Modify the file name displayed to just be base path, otherwise full path..
                " Custom file name if new file
                let s .= (bufname == '' ? '[No Name]' : fnamemodify(bufname, ':t'))

                " Marker if buffer has been modified
                let s .= (bufmodified ? '* ' : ' ')

                endfor

                let s .= '%#TabLineFill#'
                return s
                endfunction
            set tabline=%!Tabline()
        ]]

    end,
    once = true,
    group = MyAuGroup
})
