function map(modes, lhs, rhs, opts)
    local options = { noremap=true, silent=true, nowait=true }
    local modes = (type(modes) == 'table') and modes or {modes}
    if opts then
        options = vim.tbl_extend("force", options, opts)
    end

    for _, mode in pairs(modes) do
        vim.keymap.set(mode, lhs, rhs, options)
    end
end


function remap(modes, lhs, rhs, opts)
  local options = { silent=true, nowait=true }
  local modes = (type(modes) == 'table') and modes or {modes}
  if opts then
    options = vim.tbl_extend("force", options, opts)
  end

  for _, mode in pairs(modes) do
    vim.api.nvim_set_keymap(mode, lhs, rhs, options)
  end
end


function plugin_installed(plugin)
    local status_ok, _ = pcall(require, plugin)
    if not status_ok then
        return
    end
    return true
end


MyAuGroup = vim.api.nvim_create_augroup('MyGroup', {})
