vim.api.nvim_create_autocmd('VimEnter', {
    desc = 'Set highlighting for trailing spaces',
    callback = function()
        vim.api.nvim_set_hl(0, 'TrailingSpaces', { underline=true, ctermfg=9, fg='#ff0000' })
    end,
    group = MyAuGroup
})

vim.api.nvim_create_autocmd({ 'InsertLeave', 'BufEnter', 'BufWinEnter', 'WinEnter' }, {
    desc = 'Match trailing spaces pattern with the highlight',
    command = 'match TrailingSpaces /\\s\\+\\%#\\@<!$/',
    group = MyAuGroup
})

