return {
    'smoka7/hop.nvim',
    event='VeryLazy',
    config=function()
        local hop = require'hop'
        hop.setup()

        local hop_hint = require'hop.hint'

        map('', '<leader>f', function() hop.hint_char1({ direction=hop_hint.HintDirection.AFTER_CURSOR, inclusive_jump=true }) end)
        map('', '<leader>t', function() hop.hint_char1({ direction=hop_hint.HintDirection.AFTER_CURSOR, hint_offset=-1 }) end)
        map('', '<leader>F', function() hop.hint_char1({ direction=hop_hint.HintDirection.BEFORE_CURSOR, inclusive_jump=true }) end)
        map('', '<leader>T', function() hop.hint_char1({ direction=hop_hint.HintDirection.BEFORE_CURSOR, hint_offset=1 }) end)
        map('', '<leader>b', function() hop.hint_words({ direction=hop_hint.HintDirection.BEFORE_CURSOR, inclusive_jump=true }) end)
        map('', '<leader>w', function() hop.hint_words({ direction=hop_hint.HintDirection.AFTER_CURSOR, inclusive_jump=true }) end)
        map('', '<leader>/', function() hop.hint_patterns({ direction=hop_hint.HintDirection.AFTER_CURSOR }) end)
        map('', '<leader>?', function() hop.hint_patterns({ direction=hop_hint.HintDirection.BEFORE_CURSOR }) end)
        map('', '<leader>e', function() hop.hint_words({ direction=hop_hint.HintDirection.AFTER_CURSOR, hint_position=require'hop.hint'.HintPosition.END }) end)
        map('', '<leader>ge', function() hop.hint_words({ direction=hop_hint.HintDirection.BEFORE_CURSOR, hint_position=require'hop.hint'.HintPosition.END }) end)
    end
}

