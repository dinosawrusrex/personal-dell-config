local highlights = {
    'RainbowDelimiterYellow',
    'RainbowDelimiterCyan',
    'RainbowDelimiterViolet',
    'RainbowDelimiterGreen',
    'RainbowDelimiterOrange',
}

return {
    {
        'HiPhish/rainbow-delimiters.nvim',
        main = 'rainbow-delimiters.setup',
        init = function()
            vim.api.nvim_set_hl(0, 'RainbowDelimiterYellow', { ctermfg=226, fg='#ffff00' })
            vim.api.nvim_set_hl(0, 'RainbowDelimiterCyan', { ctermfg=45, fg='#00d7ff' })
            vim.api.nvim_set_hl(0, 'RainbowDelimiterViolet', { ctermfg=170 })
            vim.api.nvim_set_hl(0, 'RainbowDelimiterGreen', { ctermfg=40 })
            vim.api.nvim_set_hl(0, 'RainbowDelimiterOrange', { ctermfg=210 })
        end,
        opts = {highlight = highlights}
    },
    {
        'lukas-reineke/indent-blankline.nvim',
        init = function()
            vim.api.nvim_set_hl(0, 'IblIndent', { ctermfg=238, fg='#444444' })
        end,
        main = 'ibl',
        opts = {
            indent = {
                -- char = '┆',
                char = '│',
            },
            exclude = {
                filetypes = {'json','tex'},
                -- first three are defaults
                buftypes = {'terminal', 'nofile', 'quickfix', 'help'},
            },
            scope = {
                enabled = true,
                show_start = false,
                show_end = false,
                -- highlight = highlights
            }
        }
    }
}
-- If we want indentline to also share highlighting
-- Will need to call config instead of opts
-- local hooks = require("ibl.hooks")
-- hooks.register(
--     hooks.type.SCOPE_HIGHLIGHT,
--     hooks.builtin.scope_highlight_from_extmark
-- )
