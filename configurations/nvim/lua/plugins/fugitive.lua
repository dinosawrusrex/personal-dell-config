return {
    'tpope/vim-fugitive',
    config = function()
        vim.api.nvim_create_autocmd({'User'}, {
            desc = 'Folding for commit',
            pattern = 'FugitiveCommit',
            callback = function()
                vim.o.foldmethod = 'syntax'
            end,
            group = MyAuGroup
        })

        map('', '<leader>gp', '<cmd>diffput<cr>')
        map('', '<leader>gg', '<cmd>diffget<cr>')

        vim.api.nvim_set_hl(0, 'DiffAdd', { ctermbg=22, ctermfg=15, bg='#005f00', fg='#ffffff' })
        vim.api.nvim_set_hl(0, 'DiffDelete', { ctermbg=52, ctermfg=15, bg='#5f0000', fg='#ffffff' })
        vim.api.nvim_set_hl(0, 'DiffText', { ctermbg=130, ctermfg=15, bg='#af5f00', fg='#ffffff' })

        vim.api.nvim_set_hl(0, 'fugitiveUnstagedModifier', { link='Comment' })
        vim.api.nvim_set_hl(0, 'fugitiveUnstagedHeading', { link='fugitiveSymbolicRef' })

        vim.api.nvim_set_hl(0, 'fugitiveStagedModifier', { link='Comment' })
        vim.api.nvim_set_hl(0, 'fugitiveStagedHeading', { link='fugitiveSymbolicRef' })

        vim.api.nvim_set_hl(0, 'fugitiveUntrackedModifier', { link='Comment' })
        vim.api.nvim_set_hl(0, 'fugitiveUntrackedHeading', { link='fugitiveSymbolicRef' })

        vim.api.nvim_set_hl(0, 'fugitiveCount', { link='Comment' })

        vim.api.nvim_set_hl(0, 'diffLine', { link='WarningMsg' })
        vim.api.nvim_set_hl(0, 'diffSubname', { link='Comment' })
    end
}
