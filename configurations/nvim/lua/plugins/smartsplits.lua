return {
    'mrjones2014/smart-splits.nvim',
    keys = {
        {'<c-w>j', function() require('smart-splits').swap_buf_down({ move_cursor = true }) end, 'n'},
        {'<c-w>k', function() require('smart-splits').swap_buf_up({ move_cursor = true }) end, 'n'},
        {'<c-w>h', function() require('smart-splits').swap_buf_left({ move_cursor = true }) end, 'n'},
        {'<c-w>l', function() require('smart-splits').swap_buf_right({ move_cursor = true }) end, 'n'},
    },
    config = true
}
