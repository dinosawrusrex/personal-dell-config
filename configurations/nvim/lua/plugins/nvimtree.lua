return {
    'kyazdani42/nvim-tree.lua',
    keys = {
        {'<f3>', function() require('nvim-tree.api').tree.toggle() end, 'n'}
    },
    config = function()
        local function on_attach(bufnr)

            local api = require('nvim-tree.api')

            local function opts(desc)
                return { desc = 'nvim-tree: ' .. desc, buffer = bufnr }
            end

            map('n', '<C-]>', api.tree.change_root_to_node, opts('CD'))
            map('n', '<C-k>', api.node.show_info_popup, opts('Info'))

            map('n', 'R', api.fs.rename_full, opts('Rename: Full Path'))
            map('n', 'r', api.fs.rename, opts('Rename'))

            map('n', '<CR>',  api.node.open.edit, opts('Open'))
            map('n', '<2-LeftMouse>', api.node.open.edit, opts('Open'))
            map('n', '<C-t>', api.node.open.tab, opts('Open: New Tab'))
            map('n', '<C-v>', api.node.open.vertical, opts('Open: Vertical Split'))
            map('n', '<C-s>', api.node.open.horizontal, opts('Open: Horizontal Split'))

            map('n', 'J', api.node.navigate.sibling.last, opts('Last Sibling'))
            map('n', 'K', api.node.navigate.sibling.first, opts('First Sibling'))
            map('n', '>', api.node.navigate.sibling.next, opts('Next Sibling'))
            map('n', '<', api.node.navigate.sibling.prev, opts('Previous Sibling'))
            map('n', '[c', api.node.navigate.git.prev, opts('Prev Git'))
            map('n', ']c', api.node.navigate.git.next, opts('Next Git'))

            map('n', 'D', api.fs.remove, opts('Delete'))
            map('n', 'd', api.fs.trash, opts('Trash'))

            map('n', '.', api.node.run.cmd, opts('Run Command'))
            map('n', 's', api.node.run.system, opts('Run System'))

            map('n', '-', api.tree.change_root_to_parent, opts('Up'))

            map('n', 'y', api.fs.copy.filename, opts('Copy Name'))
            map('n', 'Y', api.fs.copy.absolute_path, opts('Copy Absolute Path'))

            map('n', 'x', api.fs.cut, opts('Cut'))
            map('n', 'p', api.fs.paste, opts('Paste'))

            map('n', 'a', api.fs.create, opts('Create'))

            map('n', 'F', api.live_filter.clear, opts('Clean Filter'))
            map('n', 'f', api.live_filter.start, opts('Filter'))
            map('n', 'B', api.tree.toggle_no_buffer_filter, opts('Toggle No Buffer'))
            map('n', 'C', api.tree.toggle_git_clean_filter, opts('Toggle Git Clean'))
            map('n', 'H', api.tree.toggle_hidden_filter, opts('Toggle Dotfiles'))
            map('n', 'I', api.tree.toggle_gitignore_filter, opts('Toggle Git Ignore'))
            map('n', 'U', api.tree.toggle_custom_filter, opts('Toggle Hidden'))

            map('n', 'g?', api.tree.toggle_help, opts('Help'))
        end

        require('nvim-tree').setup({
            open_on_tab = true,
            hijack_cursor = true,
            on_attach=on_attach,
            view = {
                signcolumn = 'no',
                relativenumber = true,
            },
            renderer = {
                icons = {
                    show = {
                        file = false,
                        folder = false,
                        folder_arrow = false,
                        git = false
                    }
                }
            },
            sync_root_with_cwd = true,
            reload_on_bufenter = true,
            respect_buf_cwd = true,
            update_focused_file = {
                enable = true,
                update_root = true
            },
            filters = {
                -- Hide dotfiles by default
                dotfiles = true,
            },
            git = {
                -- Can do without git signs
                enable = false,
            },
        })

        vim.api.nvim_set_hl(0, 'NvimTreeWindowPicker', { link='Visual' })
        vim.api.nvim_set_hl(0, 'NvimTreeCursorLine', { link='PmenuSel' })
        vim.api.nvim_set_hl(0, 'NvimTreeCursorLineNr', { link='LineNr' })
    end
}


