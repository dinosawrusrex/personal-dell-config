return {
    { 'vim-scripts/restore_view.vim' },
    { 'airblade/vim-matchquote', event = 'VeryLazy' },
    -- {
    --     'MaxMEllon/vim-jsx-pretty',
    --     ft = { 'typescriptreact', 'javascriptreact' },
    --     enabled = true
    -- },
    -- { 'Glench/Vim-Jinja2-Syntax' },
}
