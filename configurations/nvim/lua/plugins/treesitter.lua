return {
    {
    'nvim-treesitter/nvim-treesitter',
    config = function()
        require('nvim-treesitter.configs').setup({
            ensure_installed = { 'python', 'lua', 'typescript', 'javascript', 'tsx', 'html', 'css', 'elixir', 'vimdoc', 'typst' },
            auto_install = false,
            highlight = {
                enable = true,
            },
            indent = {
                enable = true,
            },
            incremental_selection = {
                enable = true,
                keymaps = {
                    init_selection = '<C-space>',
                    node_incremental = '<C-space>',
                    node_decremental = '<bs>',
                    scope_incremental = false,
                }
            },
            textobjects = {
                select = {
                    enable = true,
                    lookahead = true,
                    keymaps = {
                        ['ab'] = '@block.outer',
                        ['ib'] = '@block.inner',
                        ['af'] = '@function.outer',
                        ['if'] = '@function.inner',
                        ['ac'] = '@class.outer',
                        ['ic'] = '@class.inner',
                    }
                },
                lsp_interop = {
                    enable = true,
                    border = 'none',
                    floating_preview_opts = {},
                    peek_definition_code = {
                        ['<leader>df'] = '@function.outer',
                        ['<leader>dF'] = '@class.outer',
                    },
                },
            },
            matchup = {
                enable = true,
                disable_virtual_text=true,
                include_match_words=true
            }
        })
    end
    },
    {
        'windwp/nvim-ts-autotag',
        config = true,
        ft = { 'html', 'xml', 'typescriptreact' }
    },
    {
        'andymass/vim-matchup',
        init = function()
            vim.g.matchup_matchparen_offscreen = { method='popup' }
        end
    },
    {
        'nvim-treesitter/nvim-treesitter-textobjects'
    }
}
