return {
    'nvim-lualine/lualine.nvim',
    config = function()
        local mirage = require('lualine.themes.ayu_mirage')
        mirage.normal.c.fg = '#6dc995'

        require('lualine').setup({
            options = {
                icons_enabled = false,
                theme = mirage,
                component_separators = { left='', right='' },
                section_separators = { left='', right='' },
            },
            sections = {
                lualine_a = {'mode'},
                lualine_b = {
                    {'branch', icons_enabled=true, icon=''}
                },
                lualine_c = {{
                    'filename',
                    symbols = {
                        modified = '[+]',
                        readonly = '[RO]',
                        unnamed = '[No Name]',
                    }
                }},
                lualine_x = {'filetype'},
                lualine_y = {'progress'},
                lualine_z = {'location'},
            },
            inactive_sections = {
                lualine_a = {},
                lualine_b = {},
                lualine_c = {{
                    'filename',
                    symbols = {
                        modified = '[+]',
                        readonly = '[RO]',
                        unnamed = '[No Name]',
                    }
                }},
                lualine_x = {'location'},
                lualine_y = {},
                lualine_z = {},
            },
            extensions = {'nvim-tree', 'quickfix'}
        })
    end
}
