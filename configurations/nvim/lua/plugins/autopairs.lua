return {
    'jiangmiao/auto-pairs',
    config = function()
        vim.g.AutoPairsShortcutToggle = ''
        vim.g.AutoPairsShortcutFastWrap = ''
        vim.g.AutoPairsShortcutJump = ''
        vim.g.AutoPairsShortcutBackInsert = ''

        vim.g.AutoPairsFlyMode = false
        vim.g.AutoPairsShortcutBackInsert = ''

        vim.api.nvim_create_autocmd('FileType', {
            desc = 'Add pairs for HTML and Jinja',
            pattern = 'html',
            command = [[let b:AutoPairs = AutoPairsDefine({'<!--' : '-->', '{%' : '%}', '{#' : '#}'})"]],
            group = MyAuGroup
        })
    end
}
