return {
    'xorid/swap-split.nvim',
    keys = {
        {'<c-w>m', function() require('swap-split').swap() end, 'n'}
    },
    config = function()
        require('swap-split').setup({
            ignore_filetypes = {'NvimTree'}
        })
        vim.api.nvim_set_hl(0, 'SwapSplitStatusLine', { link='Visual' })
    end
}
