return {
    'stevearc/aerial.nvim',
    keys = {
        {'<f4>', function() require('aerial').toggle() end}
    },
    init = function()
        vim.api.nvim_set_hl(0, 'AerialLine', { link='TabLineSel' })
    end,
    opts = {
        backends = { 'treesitter', 'lsp', 'markdown', 'man' },
        attach_mode = 'global',
        layout = {
            default_direction = 'right',
            placement = 'edge',
            min_width = { 30, 0.2 }
        },
        keymaps = {
            ['?'] = 'actions.show_help',
            ['<CR>'] = 'actions.jump',
            ['<C-v>'] = 'actions.jump_vsplit',
            ['<C-s>'] = 'actions.jump_split',
            ['<C-j>'] = 'actions.down_and_scroll',
            ['<C-k>'] = 'actions.up_and_scroll',
            ['('] = 'actions.prev_up',
            [')'] = 'actions.next_up',
            ['s'] = 'actions.scroll',
            ['<space>'] = 'actions.tree_toggle',
            ['o'] = 'actions.tree_toggle_recursive',
            ['q'] = 'actions.close',

            ['p'] = false,
            ['g?'] = false,
            ['<2-LeftMouse>'] = false,
            ['{'] = false,
            ['}'] = false,
            ['[['] = false,
            [']]'] = false,
            ['O'] = false,
            ['za'] = false,
            ['zA'] = false,
            ['l'] = false,
            ['zo'] = false,
            ['L'] = false,
            ['zO'] = false,
            ['h'] = false,
            ['zc'] = false,
            ['H'] = false,
            ['zC'] = false,
            ['zr'] = false,
            ['zR'] = false,
            ['zm'] = false,
            ['zM'] = false,
            ['zx'] = false,
            ['zX'] = false,
        },
        on_attach = function(bufnr) end
    }
}
