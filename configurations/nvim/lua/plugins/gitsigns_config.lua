return {
    'lewis6991/gitsigns.nvim',

    init = function()
        vim.opt.signcolumn = 'yes'
        vim.api.nvim_set_hl(0, 'GitSignsAdd', { ctermfg=41, fg='#00d75f' })
        vim.api.nvim_set_hl(0, 'GitSignsDelete', { ctermfg=203, fg='#ff5f5f' })
        vim.api.nvim_set_hl(0, 'GitSignsChange', { ctermfg=226, fg='#ffff00' })
    end,

    config = function()
        local gitsigns = require('gitsigns')

        on_attach = function(bufnr)
            map('n', '<leader>hr', gitsigns.refresh)
            map('n', '<leader>hu', gitsigns.reset_hunk)
            map('n', '<leader>hf', gitsigns.setqflist)
            map('n', '<leader>hb', gitsigns.toggle_current_line_blame)
            map('n', '<leader>hw', gitsigns.toggle_word_diff)
            map('n', '<leader>hd', gitsigns.toggle_deleted)
            map('n', '<leader>hp', gitsigns.preview_hunk)
            map('n', ']c', gitsigns.next_hunk)
            map('n', '[c', gitsigns.prev_hunk)
        end

        gitsigns.setup({
            on_attach = on_attach,
            signs = {
                add = {text = '+'},
                change = {text = '~'},
                delete = {text = '-'},
            },
            current_line_blame_opts = {
                delay = 100,
            },
            current_line_blame_formatter = '<summary> (<author> <author_time:%Y-%m-%d %H:%M>)',
        })
    end
}
