return {
    'yssl/QFEnter',
    ft = 'qf',
    config = function()
        vim.cmd[[
            let g:qfenter_keymap.vopen = ['<c-v>']
            let g:qfenter_keymap.hopen = ['<c-s>']
            let g:qfenter_keymap.topen = ['<c-t>']

            let g:qfenter_exclude_filetypes = ['NvimTree', 'aerial', 'help']
        ]]
    end
}
