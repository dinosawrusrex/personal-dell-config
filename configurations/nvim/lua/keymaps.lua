-- Make semicolon the "colon"
map('', ';', ':')
map('', ';;', ';')

-- For prev character, after f or t commands
map('', '<leader>;', ',')

-- Navigation through "lines"
map('', 'j', 'gj')
map('', 'k', 'gk')
map('', '<expr>k', "(v:count == 0 ? 'gk' : 'k')")
map('', '<expr>j', "(v:count == 0 ? 'gj' : 'j')")

-- Format line
map('n', 'Q', 'gqq')

-- Toggle fold
map('n', '<space>', 'za')

-- Jump remaps, reversing since 'i' is left of 'o' on qwerty
-- <c-i> is <tab>
map('n', '<tab>', '<c-o>')
map('n', '<c-o>', '<tab>')

-- New line while maintaining cursor position
map('n', '<leader>o', 'moo<esc>`o')
map('n', '<leader>O', 'moO<esc>`o')

-- Unmaps
map('n', '<c-f>', '<nop>')
map('n', '<c-p>', '<nop>')

map('n', '<F5>', ':setlocal spell! spelllang=en_us<enter>')

-- Command mode remap
remap('c', '<c-k>', '<c-p>')
remap('c', '<c-j>', '<c-n>')

-- Convenient indentation shifts
map('v', '<', '<gv')
map('v', '>', '>gv')

-- Unindent
map('i', '<s-tab>', '<c-d>')

-- Tabs
map('n', '<c-w>t', ':tabnew<cr>')
map('n', '<c-w><c-t>', ':tabnew %<cr>')
map('n', '<leader>gt', 'gT')

-- Split navigation
map('n', '<c-j>', '<c-w>j')
map('n', '<c-k>', '<c-w>k')
map('n', '<c-h>', '<c-w>h')
map('n', '<c-l>', '<c-w>l')

-- Resize windows
map('n', '<c-up>', ':resize +3<cr>')
map('n', '<c-down>', ':resize -3<cr>')
map('n', '<c-right>', ':vertical resize +5<cr>')
map('n', '<c-left>', ':vertical resize -5<cr>')

-- Window resizing for Mac OS, ctrl-arrow has workspace navigation
-- map('n', '<c-w><c-k>', ':resize +3<cr>')
-- map('n', '<c-w><c-j>', ':resize -3<cr>')
-- map('n', '<c-w><c-l>', ':vertical resize +5<cr>')
-- map('n', '<c-w><c-h>', ':vertical resize -5<cr>')

-- New splts
map('n', '<c-w>s', ':new<cr>')
map('n', '<c-w>v', ':vnew<cr>')

-- Remove previous search highlights
map('n', '<esc>', ':nohl<cr><esc>')

-- Mappings for navigating quickfix list
map('n', '<f6>', ':cprev<cr>')
map('n', '<f7>', ':cnext<cr>')
