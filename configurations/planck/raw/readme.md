# Planck Build Guide
1. Install QMK from package manager
2. `qmk config` to get the qmk firmware directory
3. `ln -rs` this directory to qmk_firmware/keyboards/planck/keymap/<some_identifier>
4. `qmk compile -kb planck/rev6 -km <some_identifier>`
5. Put keyboard into dfu mode
6. `dfu-util -d 0483:df11 -a 0 -s 0x08000000:leave -D <path_to_bin_file>`
