# Guide for neovim config directory.

Plugins are managed through git's submodule functions. Refer [here](https://gist.github.com/manasthakur/d4dc9a610884c60d944a4dd97f0b3560) for a more thorough guide and commands. Note that when cloning `personal-dell-config`, use the `--recursive` flag so that the submodules are cloned too. Also, use symlinks from local machine to the local repo instead of hard-copying the nvim directory.

If there are untracked content in submodule, just run `git submodule foreach git clean -f -d` [reference](https://stackoverflow.com/questions/7993413/git-submodules-with-modified-and-untracked-content-why-and-how-to-remove-it).

After installing new plugins, run `:call pathogen#helptags()`
